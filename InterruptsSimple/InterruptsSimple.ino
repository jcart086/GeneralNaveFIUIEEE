
#include "motorController.h"


volatile boolean taskDone = false;
int taskNumber =0;
Motor motor1(11,8);
Motor motor2(10,7);
Motor motor3(9,6);
Motor motor4(5,4);

motorController myController(motor1, motor2, motor3, motor4);

   
void setup() {
  Serial.begin(9600);
  pinMode(13,OUTPUT);
  pinMode(2,INPUT);
  attachInterrupt(0,stageFinished,CHANGE);
}

void loop() {
 switch(taskNumber){
  case 0:
  Serial.println("waiting forIR code");
  break;

  case 1:
  Serial.println("walking over to Press button");
  
  break;

  case 2:
  Serial.println("navigating to pressureplate");
  myController.forwards();
  break;
  
  case 3:
  Serial.println("picking up box");
  myController.backwards();
  break;
  
  default:
  Serial.println(taskNumber);
  myController.haults();
  
 }

}

void stageFinished(){
  if(taskDone){
    delay(500);
    Serial.println(taskNumber);
    digitalWrite(13,LOW);
    taskDone = false;
  }else{
    delay(500);
    digitalWrite(13,HIGH);
    taskNumber++;
    taskDone =true;
  }
}

