/*--------------------------------------------------------------------------------------------------------------------*\
| IEEE clicker                                                                                                         |
| Based on the IRsend example.                                                                                         |
| An IR LED must be connected to Arduino PWM pin 3.                                                                    |
| Refer to the rules for information on the IEEE competition protocol.                                                 |
\*--------------------------------------------------------------------------------------------------------------------*/

#include <IRremote.h>
#define BTN_0 0
#define BTN_1 1
#define BTN_2 2
#define BTN_3 4
#define BTN_4 5
#define BTN_5 6
#define BTN_6 7
#define BTN_7 10

//Globals
IRsend ir_tx;

void setup()
{
    pinMode(BTN_0, OUTPUT);
    digitalWrite(BTN_0, LOW);
      pinMode(BTN_1, OUTPUT);
    digitalWrite(BTN_1, LOW);
    pinMode(BTN_0, INPUT_PULLUP);
    pinMode(BTN_1, INPUT_PULLUP);
    pinMode(BTN_2, INPUT_PULLUP);
    pinMode(BTN_3, INPUT_PULLUP);
    pinMode(BTN_4, INPUT_PULLUP);
    pinMode(BTN_5, INPUT_PULLUP);
    pinMode(BTN_6, INPUT_PULLUP);
    pinMode(BTN_7, INPUT_PULLUP);
}

void loop()
{
    //Sends an incrementing combination, separated by 40ms
    if (!digitalRead(BTN_0)) sendCmd(0);
    if (!digitalRead(BTN_1)) sendCmd(1);
    if (!digitalRead(BTN_2)) sendCmd(2);
    if (!digitalRead(BTN_3)) sendCmd(3);
    if (!digitalRead(BTN_4)) sendCmd(4);
    if (!digitalRead(BTN_5)) sendCmd(5);
    if (!digitalRead(BTN_6)) sendCmd(6);
    if (!digitalRead(BTN_7)) sendCmd(7);
}

void sendCmd(uint8_t input)
{
  for (uint8_t i = 0; i < 10; i++) {
          ir_tx.sendIEEE(input , 8);
          delay(200);
    }
    delay(100);
}

