/*--------------------------------------------------------------------------------------------------------------------*\
| IEEE transmission test                                                                                               |
| Based on the IRsend example.                                                                                         |
| An IR LED must be connected to Arduino PWM pin 3.                                                                    |
| Refer to the rules for information on the IEEE competition protocol.                                                 |
\*--------------------------------------------------------------------------------------------------------------------*/

#include <IRremote.h>

//Globals
IRsend ir_tx;
uint8_t txOut = 0;

void setup()
{
}

void loop()
{
    //Sends an incrementing combination, separated by 40ms
    for (uint8_t i = 0; i < 10; i++) {
          ir_tx.sendIEEE(4, 8);
          delay(70);
    }

    
    /*for (uint8_t i = 0; i < 10; i++) {
        ir_tx.sendIEEE(txOut, 8);
        delay(70);
    }
    txOut++;
    if(txOut>7) txOut=0;*/
    delay(1000); //1 second delay between each signal burst
}
